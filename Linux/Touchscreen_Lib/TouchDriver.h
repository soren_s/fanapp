//Version: 1.0
//Author: Soren S. development@soren.business, Joppe Smink

#pragma once

//#include <Windows.h>

#ifndef TOUCH
#define TOUCH

#define WIN32_LEAN_AND_MEAN

#define PRESS 0x80
#define HOLD 0x81
#define RELEASE 0x82

typedef struct {
	unsigned char action;
	unsigned short int x, y;
} TouchData;

struct TouchScreen {
	DCB dcb;
	HANDLE hComm;
	DWORD lpEvtMask;
	BOOL fWaitingOnStat;
	OVERLAPPED osStatus;
	COMMTIMEOUTS timeouts;
	TouchData data;

	unsigned char activeBuf, tempBuf, i;
	DWORD dwIncomingReadSize;
};

void del(struct TouchScreen*);

void initialize(const char, struct TouchScreen*);
bool getData(struct TouchScreen*);
bool event(struct TouchScreen*);

#endif
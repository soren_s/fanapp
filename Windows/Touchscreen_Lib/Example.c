//Version: 1.0
//Author: Soren S. development@soren.business, Joppe Smink

#include "library/TouchScreen.h"
#include <stdio.h>

int main() {
	struct TouchScreen t;
	TouchData* d = &t.data;

	initialize(4, &t);

	while (1) {
		if (event(&t)) {
			if (getData(&t)) {
				printf("%x %d %d\n", d->action, d->x, d->y);
				if (d->action == RELEASE) {
					printf("\n");
				}
			}
		}	
	}

	del(&t);
}
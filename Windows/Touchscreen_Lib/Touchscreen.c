//Version: 1.0
//Author: Soren S. development@soren.business, Joppe Smink

#include "Touchscreen.h"
#include <stdio.h>

//open a port
//id - the id of the the serial port
//touchScreen - a pointer to the TouchScreen struct to write to
void initialize(const char id, struct TouchScreen* touchScreen) {
	//convert the id to the full name Windows expects
	char fullString[5];
	char idString[2];

	fullString[0] = '\0';
	idString[0] = '\0';
	sprintf(idString, "%d", id);
	strcat(fullString, "COM");
	strcat(fullString, idString);

	//open the port and set the touchScreen:s pointer to this object
	touchScreen->hComm = CreateFileA(fullString, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (touchScreen->hComm == INVALID_HANDLE_VALUE) {
		printf("CreateFle failed with error code: %x", GetLastError());
		del(touchScreen);
		return;
	}

	//secure the memory and set teh settings for the serial port such as parity or the frequency rate
	touchScreen->lpEvtMask = EV_RXCHAR;
	touchScreen->fWaitingOnStat = FALSE;
	SecureZeroMemory(&touchScreen->osStatus, sizeof(OVERLAPPED));
	SecureZeroMemory(&touchScreen->timeouts, sizeof(COMMTIMEOUTS));
	SecureZeroMemory(&touchScreen->dcb, sizeof(DCB));

	if (GetCommState(touchScreen->hComm, &touchScreen->dcb)) {
		touchScreen->dcb.DCBlength = sizeof(DCB);
		touchScreen->dcb.BaudRate = CBR_9600;
		touchScreen->dcb.ByteSize = 8;
		touchScreen->dcb.Parity = NOPARITY;
		touchScreen->dcb.StopBits = ONESTOPBIT;
	}
	if (!SetCommState(touchScreen->hComm, &touchScreen->dcb)) {
		printf("SetCommState failed with error code: %x", GetLastError());
		del(touchScreen);
		return;
	}

	if (GetCommTimeouts(touchScreen->hComm, &touchScreen->timeouts)) {
		touchScreen->timeouts.ReadIntervalTimeout = 50;
		touchScreen->timeouts.ReadTotalTimeoutConstant = 50;
		touchScreen->timeouts.ReadTotalTimeoutMultiplier = 10;
		touchScreen->timeouts.WriteTotalTimeoutConstant = 50;
		touchScreen->timeouts.WriteTotalTimeoutMultiplier = 10;
	}
	if (!SetCommTimeouts(touchScreen->hComm, &touchScreen->timeouts)) {
		printf("SetCommTimeouts failed with error code: %x", GetLastError());
		del(touchScreen);
	}

	if (!SetCommMask(touchScreen->hComm, EV_RXCHAR)) {
		printf("SetCommMask failed with error code: %x", GetLastError());
		del(touchScreen);
		return;
	}

	touchScreen->osStatus.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (touchScreen->osStatus.hEvent == NULL) {
		printf("Creating event failed with error code: %x", GetLastError());
		del(touchScreen);
		return;
	}

	//initialize the variables you'll be reading
	touchScreen->i = 0;
	touchScreen->data.action = 0;
	touchScreen->data.x = 0;
	touchScreen->data.y = 0;
}

//close the comm port, should be called to prevent memory leaks
void del(struct TouchScreen* touchScreen) {
	if (touchScreen->hComm != INVALID_HANDLE_VALUE) {
		CloseHandle(touchScreen->hComm);
		touchScreen->hComm = INVALID_HANDLE_VALUE;
	}
}

//read the data
//returns false if something went wrong
BOOL getData(struct TouchScreen* touchScreen) {
	if (ReadFile(touchScreen->hComm, &touchScreen->activeBuf, 1, &touchScreen->dwIncomingReadSize, NULL) != 0) {
		switch (touchScreen->i) {
		case 0:
			touchScreen->i++;
			touchScreen->data.action = touchScreen->activeBuf;
			break;

		case 1:
			touchScreen->i++;
			touchScreen->tempBuf = touchScreen->activeBuf;
			break;

		case 2:
			touchScreen->i++;
			touchScreen->data.x = (((short) touchScreen->activeBuf) << 8) | touchScreen->tempBuf;
			break;

		case 3:
			touchScreen->i++;
			touchScreen->tempBuf = touchScreen->activeBuf;
			break;

		case 4:
			touchScreen->i = 0;
			touchScreen->data.y = (((short) touchScreen->activeBuf) << 8) | touchScreen->tempBuf;
			return TRUE;
			break;
		}
	} else {
		printf("Reading the comm port failed with error code: %x", GetLastError());
		touchScreen->data.action = 0;
		touchScreen->data.x = 0;
		touchScreen->data.y = 0;
	}

	return FALSE;
}

//call the function and wait for a new event
BOOL event(struct TouchScreen* touchScreen) {
	return WaitCommEvent(touchScreen->hComm, &touchScreen->lpEvtMask, NULL);
}